// Import Tinytest from the tinytest Meteor package.
import { Tinytest } from "meteor/tinytest";

// Import and rename a variable exported by collection-advanced-pagination.js.
import { name as packageName } from "meteor/navybits:collection-advanced-pagination";

// Write your tests here!
// Here is an example.
Tinytest.add('collection-advanced-pagination - example', function (test) {
  test.equal(packageName, "collection-advanced-pagination");
});
